Continuous Calendar
===================

This is a simple (and ugly...) jQuery plugin to create an infinite scrolling calendar grid, not divided by months but visualized as a sequence of weeks.

Here you can find a demo: https://www.madbob.org/cc/demo.html

### Usage

```
require('continous-calendar');

$('#calendar').ContinuousCalendar();
```

### Options

| name       | usage                                                                                  | default                                                                              |
| ---        | ---                                                                                    | ---                                                                                  |
| days       | Names of days to be displayed on the header. Attention: starting day is always monday. | ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']                                    |
| months     | Names of months to be displayed on the first column. It is also followed by the year.  | ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] |
| rows       | Rows (weeks) to display in the grid.                                                   | 5                                                                                    |
| tableClass | CSS class of the containing table.                                                     | 'continuous-calendar table'                                                          |
| events     | Array of events to be disposed on the calendar.                                        | []                                                                                   |

Events are described by simple objects with a few properties:

| name       | usage                                                                                  | notes                                                                                |
| ---        | ---                                                                                    | ---                                                                                  |
| date       | The date of the event to be placed on the calendar.                                    | In YYYY-MM-DD format. Mandatory                                                      |
| className  | CSS class for the event's cell.                                                        | Defaults to 'event'                                                                  |
| url        | URL for the event's cell link.                                                         | Optional                                                                             |
| title      | The string to be displayed on the event cell.                                          | Mandatory                                                                            |

### Extended Example

```
$('#calendar').ContinuousCalendar({
	days: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
	rows: 4,
	tableClass: 'continuous-calendar my-calendar',
	events: [
		{
			date: '2020-01-21',
			title: 'Test event',
			url: 'https://www.madbob.org/',
			className: 'just-a-link',
		},
		{
			date: '2020-4-16',
			title: 'Another test',
			className: 'not-a-link',
		},
	]
});
```

### Styling

The plugin is accompanied by a sample CSS file which provides a (almost) decent style and to be used as a reference for customization. Boostrap 4 or 5 is suggested.
